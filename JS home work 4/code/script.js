class Phone{
    constructor(number, model, weight){
    this.number = number;
    this.model = model;
    this.weight = weight;
    }
    receiveCall(name) {
        console.log(`Телефонує ${name}`);
    }
    getNumber(){
        console.log(this.number)
    }
};
const phone = new Phone("+380948572647", "Iphone", "0.4");
console.log(phone); 
const samsung = new Phone("+380836423647", "Samsung", "0.4");
console.log(samsung);
const xiaomi = new Phone("+380987426378", "Xiaomi", "0.4");
console.log(xiaomi);
phone.receiveCall("Anna");
phone.getNumber();

